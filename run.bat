@echo off
echo Banco Chinook Version 1.3
echo.

if "%1"=="" goto MENU
if not exist %1 goto ERROR

set SQLFILE=%1
goto RUNSQL

:ERROR
echo O arquivo %1 não existe.
echo.
goto END

:MENU
echo Options:
echo.
echo 1. Run script_banco.sql
echo 2. Run script_banco_autoincrement.sql
echo 3. Exit
echo.
choice /c 123
if (%ERRORLEVEL%)==(1) set SQLFILE=script_banco.sql
if (%ERRORLEVEL%)==(2) set SQLFILE=script_banco_autoincrement.sql
if (%ERRORLEVEL%)==(3) goto END

:RUNSQL
echo.
echo Rodando %SQLFILE%...
sqlite3 -init %SQLFILE% %SQLFILE%ite

:END
echo.
set SQLFILE=

